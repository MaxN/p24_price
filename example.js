var uah_rate = 0;

function get_ex_rate(callback) {
    $.get('https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11', function(pb_ex_rate) {      
        pb_ex_rate.forEach(function(rate) {
            if (rate.ccy === 'USD') {
                callback(rate.sale);
            }
        });
    });
}

function convert_prices(rate) {
    var selectorList = [
        '.discount_original_price:contains($)',
        '.discount_final_price:contains($)',
        '.game_purchase_price:contains($)',
        'div[class=price]:contains($)',
        '.col.search_price.discounted strike:contains($)',
        '.col.search_price:contains($):not(.discounted)',
        '.global_action_link:contains($)',
        '.savings:contains($)'];


    $(selectorList.join(', ')).each(function() {
        var price = $(this).text();
        price = price.replace('$', '').replace('USD', '').trim();
        if ($(this).parent().hasClass('underten')) {
            price = price.replace('До', '');
        }
        $(this).text((parseFloat(price) * rate).toFixed(2) + ' UAH');
    });

    $('.game_area_dlc_price:contains($)').each(function() {
        if (!$(this).children().length)
        {
            var price = $(this).text();
            price = price.replace('$', '').replace('USD', '').trim();
            $(this).text((parseFloat(price) * rate).toFixed(2) + ' UAH');
        }
    });

    $('.col.search_price.discounted:contains($) span').each(function() {
        var price_node = $(this).nextAll("br").get(0).nextSibling;
        var price = price_node.nodeValue.replace('$', '').replace('USD', '').trim();
        price_node.nodeValue = (parseFloat(price) * rate).toFixed(2) + ' UAH';
    });
}

get_ex_rate(function(rate) {
    uah_rate = rate;
    convert_prices(uah_rate);
});

window.addEventListener("message", function(event) {
    if (event.data === 'updatePrices') {
        convert_prices(uah_rate);
    }
}, false);


$.getScript(chrome.extension.getURL('/js/ajaxProxy.js'));

