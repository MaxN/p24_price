//списко поддерживаемых банков
//создается объект с методами rates и name
//приват24
var pb24 = {
	rates: function(callback) {
		$.getJSON("https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11")
	 		.done(function( json ) {
	 			if (callback)
	 				callback({usd: json[0].sale, euro: json[1].sale});
	  				
	  		})
	 		.fail(function(){
	 			if (callback) callback(false);
			});	
	},
	name: "Privat24"
}
