var list = [
	{
		host: "aliexpress.com",
		usd: {
			selectors: [
				'span.price span.value:contains($)',
				'div.product-price-main span.p-price',
				'div.product-price-main span.p-symbol:contains($)',
				'tr.order-head p.amount-num:contains($)',
				],
			strips: ['$', 'USD', 'US', ',']

		}
	},
	{
		host: "chainreactioncycles.com",
		usd: {
			selectors: [
				'#listview ul span.bold:contains($)',
				'#show_grid_list ul span.bold:contains($)',
				'#show_grid_list ul li.fromamt:contains($)',
				'div.content_container li.crcPDPPriceCurrent span:contains($)'
			],
			strips: ['$', ',']
		},
		euro: {
			selectors: [
				'#listview ul span.bold:contains(€)',
				'#show_grid_list ul span.bold:contains(€)',
				'#show_grid_list ul li.fromamt:contains(€)',
				'div.content_container li.crcPDPPriceCurrent span:contains(€)'
			],
			strips: ['€', ',']
		}
	},

	{
		host: "sportsdirect.com",
		usd: {
			selectors: [
				'#ProductContainer span.curprice:contains($)',
				'#productDetails span:contains($)'
			],
			strips: ['$']
		},
		euro: {
			selectors: [
				'#ProductContainer span.curprice:contains(€)',
				'#productDetails span:contains(€)'
			],
			strips: ['€'],
			replace: [ 
				{',':'.'}
			]
		}
	}
];
