function isMatchHost(host1, host2) {
	return host1.match(new RegExp(host2, 'i'))
}



function saveSwitchState(on) {
	chrome.storage.sync.set({'switchState': on});
}

function loadSwitchState(callback) {
	chrome.storage.sync.get('switchState', function(value) {
		if (chrome.runtime.lastError) {
			if (chrome.runtime.lastError.message) 
				console.log('error during loadSwitchState: '+ chrome.runtime.lastError.message)
			else
				console.log('error during loadSwitchState');

			return;
		}
		var on = true;
		if (typeof value['switchState'] !== 'undefined')
			on = value['switchState'];
		callback(on);
	});
}
