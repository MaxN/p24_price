var swch1 = true;

function turnOnSwitch(on) {
	if (!on) {
		$('#swch').text('OFF');
		$('#swch').attr('class', 'switchOff');
		$('#pct').attr('src', 'icons/48x48_disabled.png');
		$('#bank_name').attr('style', 'font-weight: bold;color:grey;');
		$('#usd').attr('style', 'color:grey;');
		$('#euro').attr('style', 'color:grey;');
		saveSwitchState(false);
		swch1 = false;

	} else {
		$('#swch').text('ON');
		$('#swch').attr('class', 'switchOn');
		$('#pct').attr('src', 'icons/48x48.png');
		$('#bank_name').attr('style', 'font-weight: bold;color:black;');
		$('#usd').attr('style', 'color:black;');
		$('#euro').attr('style', 'color:black;');
		saveSwitchState(true);
		swch1 = true;
	}

};

$('#swch').click(function() {
	if (swch1) {
		turnOnSwitch(false)
	}
	else {	
		turnOnSwitch(true)
	};
});

current_bank.rates(function(kurs) {
	if (kurs) {
		$('#bank_name').text(current_bank.name);
		$('#usd').text('1$ = '+kurs.usd+' UAH.');
		$('#euro').text('1€ = '+kurs.euro+' UAH.');
	}
	else
		$('#crs').text('N/A');
})

loadSwitchState(function(on){
	swch1 = on;
	turnOnSwitch(swch1);
	console.log('readed value: ' + JSON.stringify(value));
});
console.log('popup.js');